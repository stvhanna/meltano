## Release Checklist

### Publishing

- [ ] Record [Release Process](https://about.gitlab.com/handbook/meltano/engineering/#release-process) to kick off pipelines
  - [ ] Upload to Meltano YouTube channel using the [Meltano YouTube guidelines](https://about.gitlab.com/handbook/meltano/marketing/#youtube)
- [ ] Verify release was published successfully to [PyPi](https://pypi.org/project/meltano/)
- [ ] Verify users can successfully install new version on local machine
- [ ] Follow [DigitalOcean publish process](https://about.gitlab.com/handbook/meltano/engineering/#digitalocean-marketplace)
- [ ] If a non-patch release, record [Speedrun Video](https://about.gitlab.com/handbook/meltano/engineering/#speedruns):
  - [ ] Upload to Meltano YouTube channel using the [Meltano YouTube guidelines](https://about.gitlab.com/handbook/meltano/marketing/#youtube)
- [ ] Announce release in Meltano Slack @channel in #announcements

### Marketing

TBD
