---
sidebar: auto
---

# Meltano Handbook

## Customer Support

Emails to `hello@meltano.com` forward to our Zendesk inbox so they can be triaged, assigned, and managed. We get a wide range of inbound there, including people who are looking for support and account setups. Login info for each member of the Meltano team has been sent out, and the master account login info is in 1Password.

### Responsible Disclosure of Security Vulnerabilities

Emails to `security@meltano.com` also forward to our Zendesk inbox, and are automatically assigned to the Security group which consists of Danielle, Douwe, Micael, and Yannis. As documented in our [Responsible Disclosure Policy](/docs/responsible-disclosure.md), we will acknowledge receipt of a vulnerability report the next business day and strive to send the reporter regular updates about our progress.

## Engineering

- [MeltanoData Guide](/handbook/engineering/meltanodata-guide/)
